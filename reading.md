# 《鲤享》小程序UI讲解

> <span style="color:blue">作者：梁永豪</span>（兼-负责人）
>
> 小组成员：陈乾锋、张荣辉、张迅恒、叶卓毅

```css
├─Components（组件目录）
│  ├─copyRight（2021 鲤鱼科技《底部分割线内容》）
│  └─webComponent（公众号页面）
├─custom-tab-bar（底部导航）
├─image（轮播图片存放）
├─pages（页面存放包）
│  ├─index（首页）
│  ├─classify（分类）
│  ├─pointsExchange（积分总换）
│  ├─user（个人中心）
│  ├─userSetting（个人信息设置）
│  ├─userLogin（用户登录）
│  ├─star（个人中心-收藏）
│  ├─points（个人中心-积分）
│  ├─setting_msg（个人中心-信息）
│  ├─setting_feedback（个人中心-留言反馈）
│  ├─payRecord（个人中心-积分总换记录）
│  ├─about（个人中心-关于我们）
│  ├─contact（个人中心-联系我们）
│  ├─search（分类-搜索《暂未完善》）
│  └─content（待用--内容渲染）
│  └─test(测试页面)
```

#### 讲解

##### 1、陈乾锋

```css
├─pages（页面存放包）
│  ├─index（首页）
```

![](E:\鲤享小程序开发\index.jpg)

1. 轮播图大家都知道，这里就不多讲了
2. 通知栏：采用弹性布局；一个小喇叭图标，一串文字，文字会重复滚动，滚动采用有屏幕监听器和定时器(setInterval)，每秒向左移动
3. 推荐小程序也采用弹性布局分为每行三个存放，点击则会跳转到其他小程序

![](E:\鲤享小程序开发\index_other.jpg)

1. 这里推荐采用网页中的手风琴模式，点击后才会展开，下面每个内容点击则会出现复制按钮，出现按钮采用css中的 伪元素《:hover》



#### 2、分类页面

```css
├─pages（页面存放包）
│  ├─classify（分类）
│  ├─search（分类-搜索《暂未完善》）
```

![](E:\鲤享小程序开发\classfiy.jpg)

1. tabs：类似bootstrap4中的tabs点击在两个内容展示区域进行切换

2. 内容展示区域：手指向左滑动则有个收藏按钮，点击后：

   ![](E:\鲤享小程序开发\classfiy_click.jpg)

3. 搜索图标点击则跳转到搜索页面，搜索页面顶部有个返回点击事件，点击返回

![](E:\鲤享小程序开发\search.jpg)



#### 3、积分总换页面

```css
├─pages（页面存放包）
│  ├─pointsExchange（积分总换）
```

![](E:\鲤享小程序开发\pointsExchange.jpg)

1. 该页面有手风琴模式
2. 下面图片可以随意左右移动：采用` <scroll-view class="img_list" scroll-x="true"></scroll-view>`
3. 点击未实现



##### 4、个人中心页面

```css
├─pages（页面存放包）
│  ├─user（个人中心）
│  ├─userSetting（个人信息设置）
│  ├─userLogin（用户登录）
│  ├─star（个人中心-收藏）
│  ├─points（个人中心-积分）
│  ├─setting_msg（个人中心-信息）
│  ├─setting_feedback（个人中心-留言反馈）
│  ├─payRecord（个人中心-积分总换记录）
│  ├─about（个人中心-关于我们）
│  ├─contact（个人中心-联系我们）
```

![](E:\鲤享小程序开发\user.jpg)

![](E:\鲤享小程序开发\login.jpg)

1. 登录后效果

![](E:\鲤享小程序开发\login_after.jpg)





