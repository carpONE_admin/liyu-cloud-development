//app.js
App({
  setGlobalUserInfo: function (userInfo) {
    //console.log(userInfo)
    wx.setStorageSync('userInfo', userInfo)
  },
  getGlobalUserInfo: function () {
    return wx.getStorageSync('userInfo')
  },
  
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力');
    } else {
      wx.cloud.init({
        env: 'cloud1-0go5obgu7b9b4d83',
        traceUser: true,
      });
    }
  },
});


