/**
 * 视频详情数据
 */
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'cloud1-0go5obgu7b9b4d83'
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  const db = cloud.database();
  const video = db.collection('video');
  const videoComment = db.collection('videoComment');
  let videoData, commentData;
  if (event.msg == 1) {
    videoData = await video.where({
      videoId: event.videoId
    }).get();

    commentData = await videoComment.aggregate().match({
      videoId: event.videoId
    }).skip(0).limit(5).end();

    return {
      videoData,
      commentData
    }
  } else if (event.msg == 2) {
    commentData = await videoComment.aggregate().match({
      videoId: event.videoId
    }).skip(event.limitNum).limit(5).end();

    return {
      commentData
    }
  }

}