const app = getApp()
Page({
  data: {
    userName: app.getGlobalUserInfo().nickName,
    activeNames: ['0'],
  },
  /**
   * 九宫格点击事件
   */
  grid_click({ currentTarget }) {
    //console.log(currentTarget.dataset.appid)
    if (this.data.userName != null) {
      wx.navigateToMiniProgram({
        appId: currentTarget.dataset.appid,
      })

    } else {
      wx.showToast({
        icon: 'error',
        title: '你尚未登录',
        duration: 2000
      })
    }
  },



  /** 
   * 推荐列表的手风琴
  */
  collapseChange(event) {
    this.setData({
      activeNames: event.detail,
    });
  },
  /** 
   * 推荐列表的点击事件
  */
  recommend({ currentTarget }) {
    if (this.data.userName != null) {
      if (currentTarget.dataset.iscopy) {
        wx.navigateToMiniProgram({
          appId: currentTarget.dataset.link,
        })
      } else {
        wx.setClipboardData({
          data: currentTarget.dataset.link,
          success(res) {
            wx.showToast({
              title: '复制成功',
            })
          },
          fail(res) {
            wx.showToast({
              icon: 'error',
              title: '复制失败',
            })
          }
        })
      }
    } else {
      wx.showToast({
        icon: 'error',
        title: '你尚未登录',
        duration: 2000
      })
    }
  },

  onLoad() {
    wx.showLoading({
      title: '加载中...',
    })
    
  },

  onShow() {
    this.getTabBar().init();
    const that = this;
    wx.cloud.callFunction({
      name: 'indexDatabaseSearch',
      success(res) {
        that.setData({
          gridData: res.result.gridData.data,
          recommendData: res.result.recommendData.data,
          userName: app.getGlobalUserInfo().wxName,
          swiperImgData:res.result.swiperImgData.data
        });
        wx.hideLoading()
      }
    })
  },
  /**轮播图点击事件 */
  swiperTap() {
    wx.navigateTo({
      url: '/pages/about/about',
    })
  }
})