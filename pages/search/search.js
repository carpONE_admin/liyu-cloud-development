const db = wx.cloud.database()
Page({
  data: {
    search_value: '',
    CardContent: ""
  },
  ClickLeftBack() {
    wx.navigateBack({
      delta: 1,
    })
  },
  /**
    * 搜索框右侧的取消点击事件
    */
  search_Cancel() {
    this.setData({
      search_value: ""
    });
  },
  /**
   * 搜索框内的值改变时的事件
   */
  search_change(event) {
    this.setData({
      value: event.detail
    });
  },
  onSearch() {
    let that = this;
    wx.showLoading({
      title: '加载中...',
    })
    db.collection('classfiyContent')
      .where({
        abstract: db.RegExp({
          regexp: this.data.value,
          options: 's'
        })
      })
      .get({
        success(res) {
          that.setData({
            CardContent: res.data
          });
          wx.hideLoading()
        }
      })

  },
  /**点击后跳转 */
  ToContent({ currentTarget }) {
    wx.navigateTo({
      url: '/pages/content/content?articleId=' + currentTarget.dataset.articleid,
    })
  },
})