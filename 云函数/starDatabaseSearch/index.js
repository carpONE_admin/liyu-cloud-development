// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'cloud1-0go5obgu7b9b4d83'
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db = cloud.database();
  const collect = db.collection('collect')
  let cardData = await collect.where({
    _openid: wxContext.OPENID
  }).get()
  return {
    cardData
  }
}