const db = wx.cloud.database();
const points = db.collection('points');
const app = getApp();
let limitNum = 0;
Page({
  data: {
    current: 1
  },
  Back() {
    wx.navigateBack({
      detail: 1,
    })
  },


  onLoad(options) {
    wx.showLoading({
      title: '加载中...',
    })
    var that = this;
    if (app.getGlobalUserInfo()) {
      wx.cloud.callFunction({
        name: "login"
      }).then(res => {
        //积分
        points.aggregate()
          .match({
            _openid: res.result.openid
          }).skip(limitNum).limit(5).end({
            success(res) {
              that.setData({
                pointsContent: res.list,
                page: Math.ceil(options.count / 5),
                isShow: true
              });
              wx.hideLoading()
            }
          })
      })
    } else {
      wx.showToast({
        icon: 'error',
        title: '未登录状态',
      })
      this.setData({
        isShow: false
      })
    }

  },
  handleChange({ detail }) {
    var that = this;
    const type = detail.type;
    if (type === 'next') {
      limitNum += 5
      wx.cloud.callFunction({
        name: "login"
      }).then(res => {
        points.aggregate()
          .match({
            _openid: res.result.openid
          }).skip(limitNum).limit(5).end().then(res => {
            that.setData({
              pointsContent: res.list,
              current: this.data.current + 1
            })
          });
      })
    } else if (type === 'prev') {
      limitNum -= 5
      wx.cloud.callFunction({
        name: "login"
      }).then(res => {
        points.aggregate()
          .match({
            _openid: res.result.openid
          }).skip(limitNum).limit(5).end().then(res => {
            that.setData({
              pointsContent: res.list,
              current: this.data.current - 1
            })
          });
      })
    }
  }
})
/**计算总积分 */

function total(canArr) {
  var count = 0;
  for (var i = 0; i < canArr.length; i++) {
    count += canArr[i].addORminus
  }
  return (count);
}

