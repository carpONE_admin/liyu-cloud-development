# 《鲤享》小程序

> 本文引导：
>
> [TOC]

### 目录树

````html
D:.
├─Components        
│  ├─copyRight      
│  └─webComponent   
├─copyRight
├─custom-tab-bar    
├─image
├─miniprogram_npm   
│  ├─delegate
│  ├─good-listener
│  ├─image
│  ├─iviewWeapp
│  │  ├─action-sheet
│  │  ├─alert
│  │  ├─avatar
│  │  ├─badge
│  │  ├─base
│  │  ├─button
│  │  ├─card
│  │  ├─cell
│  │  ├─cell-group
│  │  ├─checkbox
│  │  ├─checkbox-group
│  │  ├─col
│  │  ├─collapse
│  │  ├─collapse-item
│  │  ├─count-down
│  │  ├─divider
│  │  ├─drawer
│  │  ├─grid
│  │  ├─grid-icon
│  │  ├─grid-item
│  │  ├─grid-label
│  │  ├─icon
│  │  ├─index
│  │  ├─index-item
│  │  ├─input
│  │  ├─input-number
│  │  ├─load-more
│  │  ├─message
│  │  ├─modal
│  │  ├─notice-bar
│  │  ├─page
│  │  ├─panel
│  │  ├─progress
│  │  ├─radio
│  │  ├─radio-group
│  │  ├─rate
│  │  ├─row
│  │  ├─slide
│  │  ├─spin
│  │  ├─step
│  │  ├─steps
│  │  ├─sticky
│  │  ├─sticky-item
│  │  ├─swipeout
│  │  ├─switch
│  │  ├─tab
│  │  ├─tab-bar
│  │  ├─tab-bar-item
│  │  ├─tabs
│  │  ├─tag
│  │  └─toast
│  ├─select
│  ├─tiny-emitter
│  └─weapp
│      ├─action-sheet
│      ├─area
│      ├─button
│      ├─calendar
│      │  └─components
│      │      ├─header
│      │      └─month
│      ├─card
│      ├─cell
│      ├─cell-group
│      ├─checkbox
│      ├─checkbox-group
│      ├─circle
│      ├─col
│      ├─collapse
│      ├─collapse-item
│      ├─common
│      │  └─style
│      │      └─mixins
│      ├─config-provider
│      ├─count-down
│      ├─datetime-picker
│      ├─definitions
│      ├─dialog
│      ├─divider
│      ├─dropdown-item
│      ├─dropdown-menu
│      ├─empty
│      ├─field
│      ├─goods-action
│      ├─goods-action-button
│      ├─goods-action-icon
│      ├─grid
│      ├─grid-item
│      ├─icon
│      ├─image
│      ├─index-anchor
│      ├─index-bar
│      ├─info
│      ├─loading
│      ├─mixins
│      ├─nav-bar
│      ├─notice-bar
│      ├─notify
│      ├─overlay
│      ├─panel
│      ├─picker
│      ├─picker-column
│      ├─popup
│      ├─progress
│      ├─radio
│      ├─radio-group
│      ├─rate
│      ├─row
│      ├─search
│      ├─share-sheet
│      ├─sidebar
│      ├─sidebar-item
│      ├─skeleton
│      ├─slider
│      ├─stepper
│      ├─steps
│      ├─sticky
│      ├─submit-bar
│      ├─swipe-cell
│      ├─switch
│      ├─tab
│      ├─tabbar
│      ├─tabbar-item
│      ├─tabs
│      ├─tag
│      ├─toast
│      ├─transition
│      ├─tree-select
│      ├─uploader
│      └─wxs
├─pages
│  ├─about
│  ├─classify
│  ├─contact
│  ├─content
│  ├─index
│  ├─payRecord
│  ├─points
│  ├─pointsExchange
│  ├─search
│  ├─setting_feedback
│  ├─setting_msg
│  ├─star
│  ├─test
│  ├─test2
│  ├─user
│  ├─userLogin
│  └─userSetting
└─utils
````

### 一、介绍

该小程序为《鲤鱼科技》个人团队开发。专注于编程知识、工具、好文章等各种知识分享。

### 二、使用

#### 	1、界面UI使用 vant-weapp

​		<span style="color:red">注：将采用npm模式;需要在微信小程序工具中打开如下图</span>

![img](http://img.lyxalyh.top/checkNpm.png)

#### 	2、初始化

- **1.控制台进入到小程序的目录**
- **2.进行项目的初始化,成功之后便会在小程序目录先生成package.josn文件**

```bash
1 npm init -f
```

> -f表示force的意思，就是初始化全部按照默认值，不加-f则需要填写一些项目信息，因为只是做个试验就加-f 省事。

- **3.安装Vant Weapp**

>  官方文档：https://youzan.github.io/vant-weapp/#/home

​	

```bash
npm i @vant/weapp -S --production
```

- **4、配置**

1. ​	将 app.json 中的 `"style": "v2"` 去除，小程序的[新版基础组件](https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/app.html#style)强行加上了许多样式，难以覆盖，不关闭将造成部分组件样式混乱。
2. 修改 project.config.json

开发者工具创建的项目，`miniprogramRoot` 默认为 `miniprogram`，`package.json` 在其外部，npm 构建无法正常工作。

需要手动在 `project.config.json` 内添加如下配置，使开发者工具可以正确索引到 npm 依赖的位置。

```json
{
  ...
  "setting": {
    ...
    "packNpmManually": true,
    "packNpmRelationList": [
      {
        "packageJsonPath": "./package.json",
        "miniprogramNpmDistDir": "./miniprogram/"
      }
    ]
  }
}
```



- **5.构建npm，在微信开发者工具中工具选项中点击构建npm**

![img](http://img.lyxalyh.top/buildNpm.png)

![img](http://img.lyxalyh.top/buildNpming.png)

 

​					![](http://img.lyxalyh.top/buildNpmFinish.png)

 


 构建完成之后小程序目录会多出一个下图的文件夹，然后就可以直接引用里面组件了

![img](http://img.lyxalyh.top/buildNpmFinish_mulu.png)

> 若在小程序的云函数中使用npm，无需进行初始化，但是需要先安装依赖

- **6.[使用Vant Weapp](https://youzan.github.io/vant-weapp/#/quickstart)**

