const app = getApp();
Page({

  data: {
    active: 0,
    system_info: "",
    personal_info: "",
  },
  Back() {
    wx.navigateBack({
      detail: 1,
    })
  },

  onLoad() {
    wx.showLoading({
      title: '加载中...',
    })
  },
  onShow() {
    var that = this;
    wx.cloud.callFunction({
      name: 'messageDatabaseSearch',
      data: {
        msg: 1
      },
      success(res) {
        that.setData({
          system_info: res.result.system_info.data.reverse()
        });
        wx.hideLoading()
      }
    })

    if (app.getGlobalUserInfo().wxName != undefined) {
      wx.cloud.callFunction({
        name: 'messageDatabaseSearch',
        data: {
          msg: 2
        },
        success(res) {
          that.setData({
            personal_info: res.result.personal_info.data.reverse()
          });
          wx.hideLoading()
        }
      })
    } else {
      wx.showToast({
        icon: 'error',
        title: '未登录，不可查看',
      })
    }
  },
  /**下拉刷新 */
  onPullDownRefresh() {
    wx.showLoading({
      title: '刷新中...',
    })
    let that = this;
    console.log(11)
    if (app.getGlobalUserInfo().wxName != undefined) {
      wx.cloud.callFunction({
        name: 'messageDatabaseSearch',
        data: {
          msg: 2
        },
        success(res) {
          that.setData({
            personal_info: res.result.personal_info.data.reverse()
          });
          wx.hideLoading({
            success: (res) => {
              wx.stopPullDownRefresh()
              wx.showToast({
                title: '刷新成功',
                icon: 'success'
              })
            },
          })
        }
      })
    }
  },
})