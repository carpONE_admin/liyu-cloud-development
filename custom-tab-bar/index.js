Component({
  data: {
    active: 0,
    list: [
      {
        name: 'home',
        icon: "home-o",
        text: "首页",
        url: "/pages/index/index"
      },
      {
        name: 'classify',
        icon: "orders-o",
        text: "文章专区",
        url: "/pages/article/article"
      },
      {
        name: 'pointsExchange',
        icon: "video-o",
        text: "视频专区",
        url: "/pages/videoZone/videoZone"
      },
      {
        name: 'person',
        icon: "manager-o",
        text: "个人中心",
        url: "/pages/user/user"
      }]
  },
  methods: {
    onChange(event) {
      this.setData({ active: event.detail });
     // console.log(event.detail)
      wx.switchTab({
        url: this.data.list[event.detail].url
      });
    },

    init() {
      const page = getCurrentPages().pop();
     // console.log(page)
      this.setData({
        active: this.data.list.findIndex(item => item.url === `/${page.route}`)
      });
    }
  }
})