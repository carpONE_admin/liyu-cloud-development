const db = wx.cloud.database();
const feedback = db.collection('feedback')
const user = db.collection('user')
const app = getApp()
import Dialog from '../../miniprogram_npm/weapp/dialog/dialog';
import { formatTime } from '../../utils/util';
Page({
  data: {
    feedContent: ""
  },
  back() {
    wx.navigateBack({
      delta: 1,
    })
  },

  /**
   * 留言框失去焦点时触发提交弹出框
   */
  feedConfirm({ detail }) {
    var that = this;
    const beforeClose = (action) => new Promise((resolve) => {
      setTimeout(() => {
        if (action === 'confirm') {
          this.setData({
            feedContent: ""
          })
          feedback.add({
            data: {
              content: detail.value,
              createTime: formatTime(new Date())
            },
            success() {
              wx.showToast({
                icon: "success",
                title: '提交成功',
              })
              that.onShow()
              resolve(true);
            }
          })
        } else {
          // 是否拦截取消操作
          wx.showToast({
            icon: "error",
            title: '取消提交',
          })
          resolve(true);
        }
      }, 1000);
    });

    if (detail.value != "") {
      if (app.getGlobalUserInfo().wxName != undefined) {
        Dialog.confirm({
          message: "是否提交",
          confirmButtonText: "提交",
          cancelButtonText: "取消",
          beforeClose
        });
      } else {
        wx.showToast({
          icon: 'error',
          title: '未登录状态',
        })
      }
    } else {
      wx.showToast({
        icon: 'error',
        title: '内容不能为空',
      })
    }
  },

  onShow() {
    var that = this;
    feedback.get({
      success(res) {
        that.setData({
          feedbackData: res.data
        })
      }
    });
    user.get({
      success(res) {
        that.setData({
          userInfo: res.data
        })
      }
    })
  },
})