# 底部导航栏

### 一、新建

1. 在app.js同级目录下新建文件夹“custom-tab-bar”。
2. 在“custom-tab-bar”文件夹下新建index.js、index.json、index.wxml<span style="color:red">注意：文件名必须为index</span>



### 二、

- index.js

````js
Component({
  data: {
    active: 0,
    list: [
      {
        name: 'home',
        icon: "home-o",
        text: "首页",
        //--需要注意url中最前面的/
        url: "/pages/index/index"
      },
      {
        name: 'search',
        icon: "search",
        text: "搜索",
        url: "/pages/technology/technology"
      },
      {
        name: 'setting',
        icon: "setting-o",
        text: "设置",
        url: "/pages/article/article"
      },
      {
        name: 'friends',
        icon: "friends-o",
        text: "个人中心",
        url: "/pages/user/user"
      }]
  },
  methods: {
    onChange(event) {
      this.setData({ active: event.detail });
     // console.log(event.detail)
      wx.switchTab({
        url: this.data.list[event.detail].url
      });
    },

    init() {
      const page = getCurrentPages().pop();
     // console.log(page)
      this.setData({
        active: this.data.list.findIndex(item => item.url === `/${page.route}`)
      });
    }
  }
})
````



- index.wxml

````xml
<van-tabbar active="{{active}}" bind:change="onChange">
  <van-tabbar-item wx:for="{{ list }}" wx:key="index" icon="{{ item.icon }}">{{
    item.text
  }}</van-tabbar-item>
</van-tabbar>
````

- index.json (第三方包的底部导航栏样式,也可以在app.json中进行全局化则把--"component": true,--去掉)

````json
{
	"component": true,
	"usingComponents": {
		"van-tabbar": "../vant-weapp/dist/tabbar/index",
		"van-tabbar-item": "../vant-weapp/dist/tabbar-item/index"
	}
}
````

- app.json（增加以下）

````json

	"tabBar": {
		"custom": true,
		"color": "#000000",
    "selectedColor": "#1989fa",
    "backgroundColor": "#fff",
		"list": [
			{
        "pagePath": "pages/index/index",
        "text": "首页"
			},
			{
        "pagePath": "pages/technology/technology",
        "text": "搜索"
      },
      {
        "pagePath": "pages/article/article",
        "text": "设置"
      },
      {
        "pagePath": "pages/user/user",
        "text": "个人中心"
			}
		]
	},
````

- app.js

````js
App({
  onLaunch: function () {

  }
})

````

### 三、在页面上使用

在要用底部导航栏的页面上的 index.js 增加以下

````js
const app = getApp()
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getTabBar().init();
  },
````