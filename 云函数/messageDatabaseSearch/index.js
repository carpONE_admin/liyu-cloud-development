// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'cloud1-0go5obgu7b9b4d83'
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db = cloud.database();
  const systemInfo = db.collection('systemInfo')
  const personalInfo = db.collection('personalInfo')
  if (event.msg == 1) {
    let system_info = await systemInfo.get();
    return {
      system_info
    }
  } else {
    let personal_info = await personalInfo.where({
      userId: wxContext.OPENID,
    }).get();
    return {
      personal_info
    }
  }
}