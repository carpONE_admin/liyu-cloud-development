// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'cloud1-0go5obgu7b9b4d83'
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  const db = cloud.database();
  const homeGrid = db.collection('homeGrid');
  const homeRecommend = db.collection('homeRecommend');

  //九宫格内容
 let gridData=await homeGrid.get()
 //推荐
 let recommendData =await homeRecommend.get();

 return {
  gridData,recommendData
 }

}