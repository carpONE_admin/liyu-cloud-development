const db = wx.cloud.database();
const classfiyContent = db.collection('classfiyContent')
const collect = db.collection('collect');
let _ = db.command
const app = getApp();
import { formatTime } from '../../utils/util';
Page({
  data: {

  },
  back() {
    wx.navigateBack({
      delta: 1,
    })
  },

  async onLoad(options) {
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    /**是否收藏和是否点赞 */
    if (app.getGlobalUserInfo().wxName != undefined) {
      let loginRes = await wx.cloud.callFunction({
        name: "login"
      })
      this.setData({
        openid: loginRes.result.openid
      })
      let collectRes = await collect.where({
        _openid: loginRes.result.openid,
        articleId: options.articleId
      }).get()
      if (collectRes.data.length > 0) {
        this.setData({
          isCollect: collectRes.data[0].isCollect,
          isStar: collectRes.data[0].isStar
        })
      } else {
        this.setData({
          isCollect: false,
          isStar: false
        })
      }
    } else {
      this.setData({
        isCollect: false,
        isStar: false
      })
    }
    /** -------/是否收藏和是否点赞 */
    classfiyContent.where({
      articleId: options.articleId
    }).get({
      success(res) {
        wx.hideLoading();
        that.setData({
          contentData: res.data[0],
          abstract: res.data[0].abstract
        })
      }
    });

  },

  /**收藏 */
  async onCollect({ currentTarget }) {
    var that = this;
    if (app.getGlobalUserInfo().wxName != undefined) {
      if (this.data.isCollect) {
        wx.showToast({
          title: '你已经收藏了',
          icon: 'error'
        })
      } else {
        let starRes = await collect.where({
          articleId: currentTarget.dataset.articleid,
          _openid: this.data.openid
        }).get()
        if (starRes.data.length > 0) {
          collect.where({
            articleId: currentTarget.dataset.articleid,
            _openid: this.data.openid
          }).update({
            data: {
              isCollect: true
            },
            success() {
              wx.showToast({
                title: '收藏成功',
                icon: 'success'
              })
            }
          })
          this.setData({
            isCollect: true
          })
        } else {
          collect.add({
            data: {
              articleId: currentTarget.dataset.articleid,
              collectTime: formatTime(new Date()),
              abstract: this.data.abstract,
              isCollect: true,
              isStar: false,
            },
            success() {
              that.setData({
                isCollect: true
              })
              wx.showToast({
                title: '收藏成功',
                icon: 'success'
              })
            }
          })
        }
      }
    } else {
      wx.showToast({
        title: '你还未登录',
        icon: 'error'
      })
    }

  },
  /** 评论*/
  onRecommend() {
    wx.showToast({
      title: '未开发',
      icon: 'error'
    })
  },
  /**点赞 */
  async onStar({ currentTarget }) {
    var that = this;
    if (app.getGlobalUserInfo().wxName != undefined) {
      if (this.data.isStar) {
        wx.showToast({
          title: '你已点赞',
          icon: 'error'
        })
      } else {
        let starRes = await collect.where({
          articleId: currentTarget.dataset.articleid,
          _openid: this.data.openid
        }).get()
        if (starRes.data.length > 0) {
          collect.where({
            articleId: currentTarget.dataset.articleid,
            _openid: this.data.openid
          }).update({
            data: {
              isStar: true
            },
            success() {
              wx.showToast({
                title: '点赞成功',
                icon: 'success'
              });
              //点赞成功后点赞数量增加
              // classfiyContent.where({
              //   articleId: currentTarget.dataset.articleid
              // }).update({
              //   data: {
              //     starCount: _.inc(1),
              //   },
              // }).then(res => {
              //   console.log(res)
              // })
            }
          })
          this.setData({
            isStar: true
          })
        } else {
          collect.add({
            data: {
              articleId: currentTarget.dataset.articleid,
              collectTime: formatTime(new Date()),
              abstract: this.data.abstract,
              isCollect: false,
              isStar: true,
            },
            success() {
              that.setData({
                isStar: true
              })
              wx.showToast({
                title: '点赞成功',
                icon: 'success'
              });
              //点赞成功后点赞数量增加
              // classfiyContent.where({
              //   articleId: currentTarget.dataset.articleid
              // }).update({
              //   data: {
              //     starCount: _.inc(1),
              //   }
              // }).then(res=>{
              //   console.log(res)
              // })
            }
          })
        }

      }
    } else {
      wx.showToast({
        title: '你还未登录',
        icon: 'error'
      })
    }
  },
})
