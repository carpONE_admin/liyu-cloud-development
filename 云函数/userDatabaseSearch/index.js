/**
 * 用户界面数据库信息
 */
// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'cloud1-0go5obgu7b9b4d83'
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db = cloud.database();
  const points = db.collection('points');
  const collect = db.collection('collect');
  const systemInfo = db.collection('systemInfo');
  //积分
  let pointsNum = await points.where({
    _openid: wxContext.OPENID
  }).count();

  //收藏
  let collectNum = await collect.where({
    _openid: wxContext.OPENID
  }).count();

  //消息
  let setting_msgNum = await systemInfo.count();

  return {
    pointsNum,collectNum,setting_msgNum
  }
}