Page({
  data: {
    active: 1,
    tabsChangeValue: "css"
  },
  /**
   * 搜索图标的点击事件
   */
  search() {
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  /**
   * tabs变化时事件
   */
  tabsChange({ detail }) {
    this.setData({
      tabsChangeValue: detail.title
    });
  },

  onLoad() {
    wx.showLoading({
      title: '加载中',
    })
  },

  onShow() {
    this.getTabBar().init();
    var that = this;
    wx.cloud.callFunction({
      name: 'articleDatabaseSearch',
      success(res) {
        wx.hideLoading()
        that.setData({
          classify_tab: res.result.classify_tab.data,
          classfiyCardContent: res.result.classfiyCardContent.data
        });
      }
    })
  },


  ToContent({ currentTarget }) {
    wx.navigateTo({
      url: '/pages/content/content?articleId=' + currentTarget.dataset.articleid,
    })
  },
})