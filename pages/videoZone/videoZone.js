Page({
  data: {
    activeNames: ['1']
  },
  fold(e) {
    this.setData({
      activeNames: e.detail,
    })
  },

  onLoad() {
    wx.showLoading({
      title: '加载中...',
    })
  },
  // 点击图片跳转到详情页面
  toVideoContent({ currentTarget }) {
    wx.navigateTo({
      url: '/pages/videoContent/videoContent?videoId=' + currentTarget.dataset.videoid,
    })
  },
  onShow() {
    this.getTabBar().init();
    var that = this;
    wx.cloud.callFunction({
      name: 'videoZoneDatabaseSearch',
      success(res) {
        that.setData({
          videoList: res.result.videoList.data,
          videoGroupList: res.result.videoGroupList.data,
        });
        wx.hideLoading()
      }
    })
  },

})