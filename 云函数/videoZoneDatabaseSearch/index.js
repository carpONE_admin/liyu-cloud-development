/**
 * 视频列表
 */
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'cloud1-0go5obgu7b9b4d83'
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  const db = cloud.database();
  const video = db.collection('video');
  const videoGroup = db.collection('videoGroup');
  let videoList = await video.get();
  let videoGroupList = await videoGroup.get();

  return {
    videoList, videoGroupList
  }
}