/**
 * 文章内容
 */
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'cloud1-0go5obgu7b9b4d83'
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  const db = cloud.database();
  const classfiyCard = db.collection('classifyCard');
  const classfiyContent = db.collection('classfiyContent');
  let classify_tab = await classfiyCard.get();
  let classfiyCardContent = await classfiyContent.get();

  return {
    classify_tab,classfiyCardContent
  }

}