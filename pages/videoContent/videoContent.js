const db = wx.cloud.database();
const video = db.collection('video');
const videoComment = db.collection('videoComment');
const app = getApp()
import { formatTime } from '../../utils/util';
let limitNum = 0;
Page({
  data: {
    active: 0,
    particulars: null,
    sheetShow: false,
    comment: ''
  },
  Back() {
    wx.navigateBack({
      delta: 1,
    })

  },
  onLoad(options) {
    wx.showLoading({
      title: '加载中...',
    })
    let that = this;
    wx.cloud.callFunction({
      name: 'videoContentDatabaseSearch',
      data: {
        videoId: options.videoId,
        msg: 1,
      },
      success(res) {
        that.setData({
          videoData: res.result.videoData.data[0],
          commentData: res.result.commentData.list.reverse(),
          videoId: options.videoId,
        });
        /**视频详情 */
        wx.request({
          url: 'https://api.bilibili.com/x/web-interface/view?bvid=' + res.result.videoData.data[0].bvid,
          success(res) {
            // console.log(res)
            that.setData({
              particulars: res.data.data
            });
            wx.hideLoading()
          },
        })
      }
    })
  },

  /**评论图标点击事件 */
  commentIcon() {
    this.setData({
      sheetShow: true
    })
  },
  /**动作面板关闭 */
  sheetClose() {
    this.setData({
      sheetShow: false,
    })
  },
  /**评论表单完成时事件 */
  commentSubmit() {
    var that = this;
    wx.showLoading({
      title: '提交中...',
    })
    if (app.getGlobalUserInfo().wxName != undefined) {
      videoComment.add({
        data: {
          videoId: this.data.videoId,
          avatarURL: app.getGlobalUserInfo().avatarUrl,
          userName: app.getGlobalUserInfo().wxName,
          content: this.data.comment,
          createTime: formatTime(new Date())
        },
        success() {
          that.setData({
            sheetShow: false,
            comment: ''
          })
          that.next();
          wx.hideLoading({
            success: () => {
              wx.showToast({
                icon: 'success',
                title: '评论成功',
              });
            },
          })
        }
      })
    } else {
      wx.showToast({
        title: '还未登录',
        icon: 'error'
      })
    }
  },

  /**评论上一页*/
  prev() {
    var that = this;
    limitNum -= 5
    if (limitNum <= 0) {
      limitNum = 0
    }
    wx.cloud.callFunction({
      name: 'videoContentDatabaseSearch',
      data: {
        videoId: this.data.videoId,
        msg: 2,
        limitNum
      },
      success(res) {
        that.setData({
          commentData: (res.result.commentData.list).reverse(),
        });
      }
    });
  },
  /**评论下一页 */
  next() {
    var that = this;
    if ((this.data.commentData).length == 5) {
      limitNum += 5
    }
    wx.cloud.callFunction({
      name: 'videoContentDatabaseSearch',
      data: {
        videoId: this.data.videoId,
        msg: 2,
        limitNum
      },
      success(res) {
        that.setData({
          commentData: (res.result.commentData.list).reverse(),
        });
      }
    });
  },
})