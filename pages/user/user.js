// pages/user/user.js
import Dialog from '../../miniprogram_npm/weapp/dialog/dialog';
const app = getApp()
Page({
  data: {
  },

  persion_setting() {
    if (this.data.userInfo != "") {
      wx.navigateTo({
        url: '/pages/userSetting/userSetting',
      })
    } else {
      wx.showToast({
        icon: 'error',
        title: '你尚未登录',
        duration: 2000
      })
    }
  },
  star() {
    wx.navigateTo({
      url: '/pages/star/star',
    })
  },
  points() {
    wx.navigateTo({
      url: '/pages/points/points?count=' + this.data.pointsNum,
    })
  },
  message() {
    wx.navigateTo({
      url: '/pages/message/message',
    })
  },
  feedback() {
    wx.navigateTo({
      url: '/pages/feedback/feedback',
    })
  },

  /**
   * 退出登录操作
   */
  logout() {
    try {
      wx.clearStorage({
        success: (res) => {
          wx.showToast({
            title: '退出成功',
            icon: 'success',
            duration: 2000,
          });
        },
      });
    } catch (e) {

    }
    setTimeout(() => {
      this.onShow();
      this.setData({
        avatarUrl: null,
        userName: null,
      })
    }, 1000);
  },
  /**
   * 退出登录弹出框
   */
  dialogLogout() {
    //弹出框的操作
    const beforeClose = (action) => new Promise((resolve) => {
      setTimeout(() => {
        if (action === 'confirm') {
          this.logout()
          resolve(true);
        } else {
          // 是否拦截取消操作
          resolve(true);
        }
      }, 1000);
    });
    Dialog.confirm({
      message: "退出登录后，您将无法使用鲤享小程序的资源",
      confirmButtonText: "确认退出",
      cancelButtonText: "取消",
      beforeClose
    });
  },

  onLoad() {
    wx.showLoading({
      title: '加载中...',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getTabBar().init();
    var that = this;
    if (app.getGlobalUserInfo().wxName != undefined) {
      wx.cloud.callFunction({
        name: "userDatabaseSearch",
        success(res) {
          that.setData({
            collectNum: res.result.collectNum.total,
            pointsNum: res.result.pointsNum.total,
            setting_msgNum: res.result.setting_msgNum.total,
            userInfo: app.getGlobalUserInfo(),
            avatarUrl: app.getGlobalUserInfo().avatarUrl,
            userName: app.getGlobalUserInfo().wxName,
          });
          wx.hideLoading()
        }
      })
    } else {
      this.setData({
        pointsNum: 0,
        collectNum: 0,
        setting_msgNum: 0
      });
      wx.hideLoading()
    }
  },
  contact() {
    wx.makePhoneCall({
      phoneNumber: '17329990793',
    })
  },
})

