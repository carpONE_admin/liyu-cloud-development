const app = getApp();
const db = wx.cloud.database();
const collect = db.collection('collect')
Page({
  data: {
    imageURL: "https://img.yzcdn.cn/vant/logo.png",
  },
  Back() {
    wx.navigateBack({
      detail: 1
    })
  },

  onLoad(options) {
    wx.showLoading({
      title: '加载中...',
    })
    var that = this;
    if (app.getGlobalUserInfo().wxName != undefined) {
      wx.cloud.callFunction({
        name: 'starDatabaseSearch',
        success(res) {
          that.setData({
            cardData: res.result.cardData.data
          });
          wx.hideLoading()
        }
      })
    } else {
      wx.showToast({
        icon: 'error',
        title: '未登录状态',
      })
    }
  },
  toContent({ currentTarget }) {
    wx.navigateTo({
      url: '/pages/content/content?articleid=' + currentTarget.dataset.articleid,
    })
  }
})